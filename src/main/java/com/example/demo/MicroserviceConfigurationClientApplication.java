package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.cloud.context.config.annotation.RefreshScope;

@SpringBootApplication
@RefreshScope
@ConfigurationPropertiesScan
public class MicroserviceConfigurationClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(MicroserviceConfigurationClientApplication.class, args);
	}

}
